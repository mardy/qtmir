Source: qtmir
Section: libs
Priority: optional
Maintainer: UBports Developers <developers@ubports.com>
Build-Depends: cmake,
               cmake-extras (>= 1.5-4~),
               debhelper-compat (= 12),
               googletest (>= 1.6.0+svn437),
#FIXME         libcontent-hub-dev (>= 0.2),
               libfontconfig1-dev,
               libgles2-mesa-dev,
               libglib2.0-dev,
               libgsettings-qt-dev,
               libgtest-dev,
               liblttng-ust-dev,
               libmiral-dev (>= 1.2),
               libmircommon-dev (>= 0.26.0),
               libmirserver-dev (>= 0.26.0),
               libmtdev-dev,
               libprocess-cpp-dev,
               libqt5sensors5-dev,
               libqtdbusmock1-dev (>= 0.2),
               libqtdbustest1-dev (>= 0.2),
               liblomiri-app-launch-dev,
               libudev-dev,
               liblomiri-api-dev (>= 0.1.1),
               liblomiri-url-dispatcher-dev,
               libxkbcommon-dev,
               libxrender-dev,
               mir-renderer-gl-dev (>= 0.26.0),
               mirtest-dev (>= 0.26.0),
               pkg-config,
# lttng-gen-ts needs python3, but doesn't depend on it itself: bug 1359147
               python3:any,
               qtbase5-dev,
               qtbase5-private-dev,
               qtdeclarative5-dev,
               qtdeclarative5-private-dev,
               quilt,
               valgrind [amd64 arm64 armhf i386 mips64el mipsel ppc64el s390x powerpc ppc64 x32],
# libmirserver-dev should have brought this dep. Bug lp:1617435
               uuid-dev,
# mirtest pkgconfig requires these, but doesn't have a deb dependency. Bug lp:1633537
               libboost-filesystem-dev,
               libboost-system-dev,
Standards-Version: 4.5.1
Rules-Requires-Root: no
Homepage: https://github.com/ubports/qtmir

Package: qtmir
Architecture: linux-any
Multi-Arch: same
Breaks: qtmir-android (<< 0.7.0~),
        qtmir-desktop (<< 0.7.0~),
Replaces: qtmir-android (<< 0.7.0~),
          qtmir-desktop (<< 0.7.0~),
Depends: ${misc:Depends},
         ${shlibs:Depends},
Description: Qt platform abstraction (QPA) plugin for a Mir server
 QtMir is a set of Qt5 components to enable one to write a Mir server with Qt.
 It contains a QPA (Qt Platform Abstraction) plugin which creates and manages
 a Mir server. It also exposes some internal Mir functionality.

Package: qtmir-android
Architecture: armhf arm64
Multi-Arch: same
Depends: qtmir, ${misc:Depends},
Priority: optional
Section: oldlibs
Description: transitional package
  This is a transitional package. It can safely be removed.

Package: qtmir-desktop
Architecture: linux-any
Multi-Arch: same
Depends: qtmir, ${misc:Depends},
Priority: optional
Section: oldlibs
Description: transitional package
  This is a transitional package. It can safely be removed.

Package: libqtmirserver-dev
Section: libdevel
Architecture: linux-any
Multi-Arch: same
Pre-Depends: ${misc:Pre-Depends}
Depends: qtmir (= ${binary:Version}),
         ${misc:Depends},
         libmiral-dev (>= 1.3.0),
Description: Developer files for QtMir server API
 QtMir is a set of Qt5 components to enable one to write a Mir server with Qt.
 It contains a QPA (Qt Platform Abstraction) plugin which creates and manages
 a Mir server. It also exposes some internal Mir functionality.
 .
 This package contains the library headers for developers.

Package: libqtmirserver1
Architecture: linux-any
Multi-Arch: same
Pre-Depends: ${misc:Pre-Depends}
Depends: ${misc:Depends},
      ${shlibs:Depends},
Description: QtMir server API shared library
 QtMir is a set of Qt5 components to enable one to write a Mir server with Qt.
 It contains a QPA (Qt Platform Abstraction) plugin which creates and manages
 a Mir server. It also exposes some internal Mir functionality.
 .
 Contains the shared library containing QtMir server API.

Package: qml-module-qtmir
Architecture: linux-any
Multi-Arch: same
Pre-Depends: ${misc:Pre-Depends},
Depends: qtmir (= ${binary:Version}),
         ${misc:Depends},
         ${shlibs:Depends},
Provides: lomiri-application-impl,
          lomiri-application-impl-27,
Description: Qt/QML module for Lomiri specific Mir APIs
 QtMir is a set of Qt5 components to enable one to write a Mir server with Qt.
 It contains a QPA (Qt Platform Abstraction) plugin which creates and manages
 a Mir server. It also exposes some internal Mir functionality.
 .
 QtMir provides Qt/QML bindings for Mir features that are exposed through the
 qtmir QPA plugin such as Application management (start/stop/suspend/resume)
 and surface management.

Package: qtmir-tests
Section: libdevel
Architecture: linux-any
Multi-Arch: foreign
Pre-Depends: ${misc:Pre-Depends},
Depends: littler,
         lttng-tools,
         mir-test-tools,
         python3-autopilot,
         python3-babeltrace,
         python3-evdev,
         python3-mir-perf-framework,
         qml-module-lomiri-components,
         qml-module-qtmir,
         qtmir (= ${binary:Version}),
         ${misc:Depends},
         ${shlibs:Depends},
Recommends: autopilot-qt5,
Description: QtMir tests and demos
 QtMir is a set of Qt5 components to enable one to write a Mir server with Qt.
 It contains a QPA (Qt Platform Abstraction) plugin which creates and manages
 a Mir server. It also exposes some internal Mir functionality.
 .
 This package provides benchmark tests and a simple shell and client using the
 QtMir QPA.
